"""
    mapwindow(f::Function, x::AbstractArray, windowradius::Integer)

Map function `f` onto windows of radius `windowradius` of array `x`.
"""
function mapwindow(f::Function, x::AbstractArray, windowradius::Integer)
    sliceidxs = [1:(n-2*windowradius) for n in size(x)]
    [
        f(view(x, [i .+ (0:(2*windowradius)) for i in Tuple(idx)]...)) for idx in eachindex(view(x, sliceidxs...))
    ]
end

"""
    falsepad(x::BitArray)

Pad a `BitArray` with `false` around the edges (padding width of 1). 
"""
function falsepad(x::BitArray)
    x2 = falses(size(x) .+ 2)
    #TODO: without views and .= may be faster, albeit with more allocs
    @views x2[[2:(n+1) for n in size(x)]...] .= x
    x2
end

"""
    findlocalmaxima(x::AbstractArray)->(idx, val)

Find the indices and values of all local maxima.
"""
function findlocalmaxima(x::AbstractArray)
    n = ndims(x)
    localmax(k::AbstractArray) = all(k[fill(2, n)...] .>= k)
    islocalmax = falsepad(BitArray(mapwindow(localmax, x, 1)))
    idx = findall(islocalmax)
    idx, x[idx]
end

#TODO: Implement same stuff as for 1D case, as possible
"""
    findpeaks(x; height=nothing, distance = nothing)


Find the peaks in `x` and return their indices in `x`.

# Arguments
- `x::AbstractArray`: array within which we look for peaks.
- `height=nothing`: minimal height (value) for a peak.
- `distance=nothing`: minimal spacing between peaks.
"""
function findpeaks(x::AbstractArray; height = nothing, distance = nothing)
    # Find all local maxima
    peak_idx, peak_vals = findlocalmaxima(x)
    # Filter out all peaks below a given height
    if !isnothing(height)
        peakmask = peak_vals .>= height
        peak_idx, peak_vals = peak_idx[peakmask], peak_vals[peakmask]
    end
    if !isnothing(distance)
        peak_idx, peak_vals = filterpeaksbydistance_nd(peak_idx, peak_vals, distance)
    end
    peak_idx
end
