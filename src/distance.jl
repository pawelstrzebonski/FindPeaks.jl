"""
    filterpeaksbydistance(peak_idx, peak_vals, distance)->(peak_idx, peak_vals)

Filter a set of peaks so that no peaks are within `distance` of each-other,
selecting peaks in decreasing order of height.
"""
function filterpeaksbydistance(
    peak_idx::AbstractVector,
    peak_vals::AbstractVector,
    distance::Integer,
)
    idxs = sortperm(peak_vals, rev = true)
    vals, idxs = peak_vals[idxs], peak_idx[idxs]
    chosen_idxs = eltype(peak_idx)[]
    while length(idxs) > 0
        push!(chosen_idxs, idxs[1])
        peakmask = abs.(idxs .- chosen_idxs[end]) .> distance
        vals, idxs = vals[peakmask], idxs[peakmask]
    end
    peakmask = (x -> x in chosen_idxs).(peak_idx)
    peak_idx[peakmask], peak_vals[peakmask]
end

"""
    cartesiandistance(x::CartesianIndex, y::CartesianIndex)

Distance between a pair of `CartesianIndex` values.
"""
function cartesiandistance(x::CartesianIndex, y::CartesianIndex)
    sqrt(sum((Tuple(x) .- Tuple(y)) .^ 2))
end

"""
    filterpeaksbydistance_nd(peak_idx, peak_vals, distance)->(peak_idx, peak_vals)

Filter a set of peaks so that no peaks are within `distance` of each-other,
selecting peaks in decreasing order of height.
"""
function filterpeaksbydistance_nd(
    peak_idx::AbstractVector,
    peak_vals::AbstractVector,
    distance::Integer,
)
    idxs = sortperm(peak_vals, rev = true)
    vals, idxs = peak_vals[idxs], peak_idx[idxs]
    chosen_idxs = eltype(peak_idx)[]
    while length(idxs) > 0
        push!(chosen_idxs, idxs[1])
        peakmask = [cartesiandistance(i, chosen_idxs[end]) for i in idxs] .> distance
        vals, idxs = vals[peakmask], idxs[peakmask]
    end
    peakmask = (x -> x in chosen_idxs).(peak_idx)
    peak_idx[peakmask], peak_vals[peakmask]
end
