"""
    peakwidth(x, peak_idx, eval_height)

Determine the width around `x[peak_idx]` of points larger than `eval_height`.
"""
function peakwidth(x::AbstractVector, peak_idx::Number, eval_height::Number)
    # Find all points that are shorter than the eval_height on either side of the peak
    shorterleft = reverse(x[1:peak_idx]) .< eval_height
    shorterright = x[peak_idx:end] .< eval_height
    # Find the distance to the closest point that is below eval_height on either side
    # If there is none, take the distance to the edge
    left = any(shorterleft) ? findfirst(shorterleft) : peak_idx
    right = any(shorterright) ? findfirst(shorterright) : length(x) - peak_idx
    right + left
end

"""
    findpeakwidth(x, peak_idx, peak_vals; rel_height=0.5)

Find the width of each peak. Consult `scipy.signal.peak_widths`.
"""
function findpeakwidth(
    x::AbstractVector,
    peak_idx::AbstractVector,
    peak_vals::AbstractVector;
    rel_height::Number = 0.5,
)
    prominence = findpeakprominence(x, peak_idx, peak_vals)
    eval_height = @. peak_vals - prominence * rel_height
    peak_widths = [peakwidth(x, peak_idx[i], eval_height[i]) for i = 1:length(peak_idx)]
end
