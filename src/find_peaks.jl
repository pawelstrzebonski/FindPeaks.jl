"""
    findlocalmaxima(x)->(idx, val)

Find the indices and values of all local maxima.
"""
function findlocalmaxima(x::AbstractVector)
    idx = findall([false; x[1:end-2] .<= x[2:end-1] .>= x[3:end]; false])
    idx, x[idx]
end

#TODO: Implement more options, perhaps z-score filtering or automatic argument calculations
#TODO: support non-uniform grid?
#TODO: Implement min/max arguments, array arguments?
"""
    findpeaks(x; height=nothing, threshold=nothing, prominence=nothing, distance=nothing, width=nothing)


Find the peaks in `x` and return their indices in `x`.

# Arguments
- `x::AbstractVector`: array within which we look for peaks.
- `height=nothing`: minimal height (value) for a peak.
- `threshold=nothing`: minimal height difference on both sides for a peak.
- `prominence=nothing`: minimal topological prominence for a peak.
- `distance=nothing`: minimal spacing between peaks.
- `width=nothing`: minimal width of peaks.
"""
function findpeaks(
    x::AbstractVector;
    height = nothing,
    threshold = nothing,
    prominence = nothing,
    distance = nothing,
    width = nothing,
)
    # Find all local maxima
    peak_idx, peak_vals = findlocalmaxima(x)
    # Filter out all peaks below a given height
    if !isnothing(height)
        peakmask = peak_vals .>= height
        peak_idx, peak_vals = peak_idx[peakmask], peak_vals[peakmask]
    end
    if !isnothing(threshold)
        xpad = [x[1]; x; x[end]]
        peakmask =
            min.(
                xpad[peak_idx.+1] .- xpad[peak_idx],
                xpad[peak_idx.+1] .- xpad[peak_idx.+2],
            ) .>= threshold
        peak_idx, peak_vals = peak_idx[peakmask], peak_vals[peakmask]
    end
    if !isnothing(distance)
        peak_idx, peak_vals = filterpeaksbydistance(peak_idx, peak_vals, distance)
    end
    if !isnothing(prominence)
        peakmask = findpeakprominence(x, peak_idx, peak_vals) .>= prominence
        peak_idx, peak_vals = peak_idx[peakmask], peak_vals[peakmask]
    end
    if !isnothing(width)
        peakmask = findpeakwidth(x, peak_idx, peak_vals) .>= width
        peak_idx, peak_vals = peak_idx[peakmask], peak_vals[peakmask]
    end
    peak_idx
end
