"""
    findpeakprominence(x, peak_idx, peak_vals)

Find all peaks and calculate their prominence.
"""
function findpeakprominence(
    x::AbstractVector,
    peak_idx::AbstractVector,
    peak_vals::AbstractVector,
)
    @assert length(peak_idx) == length(peak_vals)
    # Pre-allocate prominence array
    proms = zeros(length(peak_idx))
    # Iteratively find prominence for each peak
    for i = 1:length(peak_idx)
        # Find indices of all peaks at least as tall as this peak
        taller = peak_idx[findall(peak_vals .> peak_vals[i])]
        # Segregate into taller peaks on left and taller peaks on right
        l = taller[taller.<peak_idx[i]]
        r = taller[peak_idx[i].<taller]
        # Find the lowest point between this peak and next highest on the left
        # If there is no taller peak, then find lowest point between peak and left boundary
        left =
            iszero(length(l)) ? minimum(x[1:peak_idx[i]]) : minimum(x[l[end]:peak_idx[i]])
        # Find the lowest point between this peak and next highest on the right
        # If there is no taller peak, then find lowest point between peak and right boundary
        right =
            iszero(length(r)) ? minimum(x[peak_idx[i]:end]) : minimum(x[peak_idx[i]:r[1]])
        # Prominence is the smaller height difference between this peak and lowest point between the next highest peak
        proms[i] = peak_vals[i] - max(left, right)
    end
    proms
end
