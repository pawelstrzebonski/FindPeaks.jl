module FindPeaks

include("distance.jl")
include("prominence.jl")
include("width.jl")
include("find_peaks.jl")
include("find_peaks_nd.jl")

export findpeaks

end
