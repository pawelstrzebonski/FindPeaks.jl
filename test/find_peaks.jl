x = 0:0.1:(7*pi)
y = sin.(x)
y2 = @. sin(x) + x / pi

@test (FindPeaks.findpeaks(y) == FindPeaks.findlocalmaxima(y)[1])

@test (FindPeaks.findpeaks(y, distance = 10) == FindPeaks.findlocalmaxima(y)[1])
@test (FindPeaks.findpeaks(y, distance = 100) == FindPeaks.findlocalmaxima(y)[1][[1, 4]])

@test (FindPeaks.findpeaks(y, height = 0) == FindPeaks.findlocalmaxima(y)[1])
@test (FindPeaks.findpeaks(y2, height = 1) == FindPeaks.findlocalmaxima(y2)[1])
@test (FindPeaks.findpeaks(y2, height = 2) == FindPeaks.findlocalmaxima(y2)[1][2:4])

@test (FindPeaks.findpeaks(y, threshold = 0) == FindPeaks.findlocalmaxima(y)[1])
@test (FindPeaks.findpeaks(y, threshold = 0.1) == [])
@test (FindPeaks.findpeaks(y .* 1000, threshold = 0.1) == FindPeaks.findlocalmaxima(y)[1])

@test (FindPeaks.findpeaks(y, prominence = 0) == FindPeaks.findlocalmaxima(y)[1])
@test (FindPeaks.findpeaks(y, prominence = 0.5) == FindPeaks.findlocalmaxima(y)[1])
@test (FindPeaks.findpeaks(y, prominence = 1) == FindPeaks.findlocalmaxima(y)[1][2:3])

@test (FindPeaks.findpeaks(y, width = 0) == FindPeaks.findlocalmaxima(y)[1])
@test (FindPeaks.findpeaks(y, width = 10) == FindPeaks.findlocalmaxima(y)[1])
@test (FindPeaks.findpeaks(y, width = 30) == FindPeaks.findlocalmaxima(y)[1][2:3])
