x = 0:0.1:(7*pi)
y = sin.(x)
px, py = FindPeaks.findlocalmaxima(y)

@test approxeq(FindPeaks.findpeakprominence(y, px, py), [0.9996, 1.9989, 1.9991, 0.9088])
