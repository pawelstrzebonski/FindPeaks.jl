x = trues(3, 4)
x2 = FindPeaks.falsepad(x)
@test x2[2:4, 2:5] == x
@test !any(x2[:, 1]) && !any(x2[:, end]) && !any(x2[1, :]) && !any(x2[end, :])

x = ones(4, 5)
x2 = FindPeaks.mapwindow(sum, x, 1)
@test x2 == fill(9, 2, 3)

x = zeros(5, 6)
x[2, 2] = x[4, 5] = 1

@test FindPeaks.findlocalmaxima(x)[1] == FindPeaks.findpeaks(x)
@test FindPeaks.findlocalmaxima(x)[1] == FindPeaks.findpeaks(x, height = 0)
@test FindPeaks.findpeaks(x, height = 0.5) == [CartesianIndex(2, 2), CartesianIndex(4, 5)]
@test FindPeaks.findpeaks(x, height = 5) == []
@test FindPeaks.findpeaks(x, distance = 1, height = 0.5) ==
      [CartesianIndex(2, 2), CartesianIndex(4, 5)]
@test FindPeaks.findpeaks(x, distance = 10, height = 0.5) == [CartesianIndex(2, 2)]
