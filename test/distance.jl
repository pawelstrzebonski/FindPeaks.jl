x = 0:0.1:(7*pi)
y = sin.(x)

@test (FindPeaks.findpeaks(y, distance = 0) == [17, 80, 142, 205])
@test (FindPeaks.findpeaks(y, distance = 0) == FindPeaks.findlocalmaxima(y)[1])
@test (FindPeaks.findpeaks(y, distance = 10) == FindPeaks.findlocalmaxima(y)[1])
@test (FindPeaks.findpeaks(y, distance = 100) == [17, 205])
