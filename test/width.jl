x = 0:0.1:(7*pi)
y = sin.(x)
px, py = FindPeaks.findlocalmaxima(y)

@test approxeq(FindPeaks.findpeakwidth(y, px, py, rel_height = 0.25), [17, 24, 24, 17])
@test approxeq(FindPeaks.findpeakwidth(y, px, py, rel_height = 0.5), [24, 35, 35, 23])
