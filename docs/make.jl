using Documenter
import FindPeaks

makedocs(
    sitename = "FindPeaks.jl",
    repo = Documenter.Remotes.GitLab("pawelstrzebonski", "FindPeaks.jl"),
    pages = [
        "Home" => "index.md",
        "Examples" =>
            ["Basic (1D) Usage" => "example.md", "Basic (2D) Usage" => "example_nd.md"],
        "References" => "references.md",
        "Contributing" => "contributing.md",
        "src/" => [
            "distance.jl" => "distance.md",
            "find_peaks.jl" => "find_peaks.md",
            "find\\_peaks\\_nd.jl" => "find_peaks_nd.md",
            "prominence.jl" => "prominence.md",
            "width.jl" => "width.md",
        ],
        "test/" => [
            "distance.jl" => "distance_test.md",
            "find\\_peaks\\_nd.jl" => "find_peaks_nd_test.md",
            "prominence.jl" => "prominence_test.md",
            "width.jl" => "width_test.md",
        ],
    ],
)
