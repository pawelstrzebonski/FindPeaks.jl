# Contributing and Development

Contributions to this package are welcomed. Please submit issues/pull-requests
on the GitLab project.

## TODOs

The following are some areas for development and improvement:

* More, better unit tests, documentation, and examples
* Optimizations
* Guidance on choosing/tuning arguments
* More and better peak finding/filtering algorithms
* Add support for non-uniformly sampled input (as relevant for some peak filter algorithms)
* Improve peak finding/filtering in arbitrarily high dimensions
