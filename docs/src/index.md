# FindPeaks.jl Documentation

`FindPeaks.jl` is a package for finding peaks in potentially noisy
data. It was originally created as a way of finding peaks in (laser)
optical spectra, but it may be applicable to other problems. This package
also seeks to provide some basic peak finding capabilities in arbitrary
higher dimension datasets.

## Installation

This package is not in the official Julia package repository, so to
install, run in Julia:

```Julia
]add https://gitlab.com/pawelstrzebonski/FindPeaks.jl
```

## Features

* Peak finding function for 1D and ND datasets
* Distance, height, threshold, width, and prominence based peak filtering in 1D
* Distance and height based peak filtering in ND
