# width.jl

We calculate the peak width as done by the
[`scipy.signal.peak_widths`](https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.peak_widths.html#scipy.signal.peak_widths)
function.

```@autodocs
Modules = [FindPeaks]
Pages   = ["width.jl"]
```
