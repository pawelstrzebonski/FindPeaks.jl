# find_peaks_nd.jl

Very basic arbitrary dimensional peak search.

```@autodocs
Modules = [FindPeaks]
Pages   = ["find_peaks_nd.jl"]
```
