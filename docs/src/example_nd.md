# Example Higher Dimensional Usage

Let's consider the following 2D dataset:

```@example waves
import Plots: heatmap, plot
import Plots: savefig # hide

x=0:0.1:10
y=0:0.1:10
z=[sin(x)*sin(y) for x=x, y=y]

heatmap(x, y, z,
	xlabel="x",
	ylabel="y",
)
savefig("waves.png"); nothing # hide
```

![A 2D dataset](waves.png)

Without any options, `findpeaks` will find all local maxima. In order
to visualize them we will create a heatmap where the peaks have a value
of one and all non-peak points will have zero value:

```@example waves
import FindPeaks

# We locate the peaks
peak_idx=FindPeaks.findpeaks(z)

peaks=zeros(size(z))
peaks[peak_idx].=1

plot(
	heatmap(x, y, z),
	heatmap(x, y, peaks),
	xlabel="x",
	ylabel="y",
)
savefig("wave_peaks.png"); nothing # hide
```

![Local maxima found](wave_peaks.png)

We can see that five peak regions have been found within the dataset
(the local maxima at the edges, however, have not been identified as
peaks).
