# find_peaks.jl

The `findpeaks` function is strongly influenced by the Python
`scipy.signal.find_peaks` function described in the SciPy
[documentation](https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.find_peaks.html).
It starts with a list of all local maxima and selects a subset of them
based on a variety of measures.

```@autodocs
Modules = [FindPeaks]
Pages   = ["find_peaks.jl"]
```
