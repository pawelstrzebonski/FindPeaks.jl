# prominence.jl

Please consult the
[Wikipedia article](https://en.wikipedia.org/wiki/Topographic_prominence)
on topological prominence.

```@autodocs
Modules = [FindPeaks]
Pages   = ["prominence.jl"]
```
