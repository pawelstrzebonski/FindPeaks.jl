# References

This package is heavily influenced by
[`scipy.signal.find_peaks`](https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.find_peaks.html).
It is not (yet?) a full clone of said function, nor does it necessarily
aim to be. However, the SciPy peak finding function seems like a good
start for implementing general purpose peak finding functionality.
