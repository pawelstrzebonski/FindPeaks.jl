# distance.jl

We go through the peaks in descending order of height, throwing out those
that are too close to a higher peak.

```@autodocs
Modules = [FindPeaks]
Pages   = ["distance.jl"]
```
