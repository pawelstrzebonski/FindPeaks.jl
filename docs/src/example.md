# Example Usage

Let's consider the following optical spectrum measurement (from a VCSEL):

```@example spectrum
import Plots: plot, scatter!
import Plots: savefig # hide

# We import the spectrum as (wavelength, power)
include("spectrum_data.jl") # hide

plot(wavelength, power,
	xlabel="Wavelength [nm]",
	ylabel="Power [dBm]",
	legend=false,
	ylim=(-90, maximum(power)+10),
)
savefig("spectrum.png"); nothing # hide
```

![VCSEL spectrum](spectrum.png)

We can see that there are a few prominent peaks in the spectrum. Without
any options, `findpeaks` will find all local maxima:

```@example spectrum
import FindPeaks

# We locate the peaks
peak_idx=FindPeaks.findpeaks(
	power,
)
peak_wavelengths, peak_powers=wavelength[peak_idx], power[peak_idx]

plot(wavelength, power,
	xlabel="Wavelength [nm]",
	ylabel="Power [dBm]",
	legend=false,
	ylim=(-90, maximum(power)+10),
)
scatter!(peak_wavelengths, peak_powers)
savefig("all_peaks.png"); nothing # hide
```

![VCSEL spectrum with all maxima marked](all_peaks.png)

However, many of those peaks are simply noise, so we want to use some
sort of peak filtering parameters to reject the peaks in the noise
and the noise within the peaks. For a full list of options and their
explanations, please consult the `findpeaks` function documentation.
With some trial and error we can adjust the function arguments to find
the following mode peaks in the spectrum:

```@example spectrum
# We locate the peaks
peak_idx=FindPeaks.findpeaks(
	power,
	height=-70,
	distance=30,
	prominence=5,
)
peak_wavelengths, peak_powers=wavelength[peak_idx], power[peak_idx]

plot(wavelength, power,
	xlabel="Wavelength [nm]",
	ylabel="Power [dBm]",
	legend=false,
	ylim=(-90, maximum(power)+10),
)
scatter!(peak_wavelengths, peak_powers)
savefig("peaks.png"); nothing # hide
```

![VCSEL spectrum with peaks marked](peaks.png)
